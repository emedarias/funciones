//llamamos a los botones "borrar primero" y "borrar ultimo" y lo guardamos en la variable botones;
const botones = Array.from(document.querySelectorAll("input[type='button']"));
//asignamos a "borrar primero" la funcion "cuadrado";
botones[0].addEventListener("click", cuadrado);
//asignamos a "borrar ultimo" la funcion "factorial";
botones[1].addEventListener("click", factorial);
//asignamos a "borrar ultimo" la funcion "area";
botones[2].addEventListener("click", area);
//asignamos a "borrar ultimo" la funcion "password";
botones[3].addEventListener("click", password);
//llamamos al campo donde vamos a mostrar los resultados obtenidos.
const pantalla = document.querySelector("#mostrar");
//Array donde se guardan los datos para comparar con los usuarios y passwords.
const usuarios = [["Emilio","lalo123"], ["Juan","supa345"]];

function cuadrado()
	{

		//Guardamos el valor que tiene el input dentro de la variable auto.
		let numero=parseInt(document.querySelector("input[type='number']").value);
		//El cuadrado es el numero multiplicado por si mismo.
		let resultado = numero*numero;
		//Enviamos el resultado a la funcion imprimir.
		imprimir(resultado);
	}


function factorial()
	{
		//Evitamos que el formulario se envie
		event.preventDefault();
		//Guardamos el valor que tiene el input dentro de la variable auto.
		let numero=parseInt(document.querySelector("input[type='number']").value);
		//Creadmos la variable donde vamos a guardar el resultado.
		let resultado;
		//Con este ciclo repetiremos la accion de multiplicar el valor guardado por su anterior, siempre y cuando no sea la primera vez que ingresa.
		for(i=numero; i>0; i--)
			{
				if(i==numero)
					{
						//Si el numero es igual a la posicion "i", se guarda el numero ingresado.
						resultado=numero;
					}
				else
					{
						//Si el numero is distinto a la posicion "i", se multiplica el valor de "i" por el resultado guardado anteriormente.
						resultado=resultado*i;
					}
			}
		//Enviamos el resultado a la funcion imprimir.
		imprimir(resultado);
	}


function area()
	{
		//Evitamos que el formulario se envie
		event.preventDefault();
		//Guardamos el valor que tiene el input dentro de la variable auto.
		let numero=parseInt(document.querySelector("input[type='number']").value);
		//Creamos la constante con el valor de PI.
		const PI = 3.14;
		//Creamos la variable donde vamos a guardar el resultado.
		let resultado;
		//Multiplicamos el valor de PI por el cuadrado del numero ingresado.
		resultado=PI*(numero*numero);
		//Enviamos el resultado a la funcion imprimir.
		imprimir(resultado);
	}


function password()
	{
		//Evitamos que el formulario se envie
		event.preventDefault();
		//Guardamos el valor que tiene el input dentro de la variable nombre.
		let nombre=document.querySelector("input[name='usuario']").value;
		//Guardamos el valor que tiene el input dentro de la variable pass.
		let pass=document.querySelector("input[name='password']").value;
		//Creamos la variable donde vamos a guardar el resultado.
		let resultado="";

		for (personas of usuarios)
			{
				//Condicion que pregunta si el nombre y password son iguales a todos los elementos guardados en el Array de "usuarios".
				if(personas[0]===nombre && personas[1]===pass)
					{
						//Si son iguales, guarda el mensaje.
						resultado="El usuario y password son correctos."
					}
			}

		//Condicion que pregunta si resultado es igual a "", de ser asi, es porque no encontro ningun password ni usuario correcto en el for.
		if(resultado==="")
			{
				//Por lo tanto, el mensaje es que NO son correctos.
				resultado="El usuario y password NO son correctos."
			}
		//Enviamos el resultado a la funcion imprimir.
		imprimir(resultado);
	}


function imprimir(dato)
	{
		//Limpiamos el parrafo donde se van a imprimir los datos.
		pantalla.innerHTML="";
		//Agregamos al parrafo el dato enviado como parametro.
		pantalla.innerHTML+=dato;
	}